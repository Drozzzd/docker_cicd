import sys
import random


def create_sorted_array(size):
    array = []
    for _ in range(size):
        array.append(random.randint(1, 100))
    array.sort()
    return array


def binary_search(arr, target):
    low, high = 0, len(arr)-1
    while low <= high:
        mid = (low + high) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            low = mid + 1
        else:
            high = mid - 1
    return -1


def main():
    if len(sys.argv) != 2:
        print("Usage: python task1.py <target_value>")
        return

    target = int(sys.argv[1])
    array = create_sorted_array(100)

    index = binary_search(array, target)
    if index != -1:
        print(f"Value {target} found at index {index} in the array.")
    else:
        print(f"Value {target} not found in the array.")


if __name__ == "__main__":
    main()
