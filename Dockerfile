FROM python:3

WORKDIR /app

COPY /FastAPI /app

RUN pip install -r ./requirements.txt

ENTRYPOINT ["python3", "task1_api.py"]
