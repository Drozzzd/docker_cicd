import sys
import random
from fastapi import FastAPI
from uvicorn import run
import redis


app = FastAPI(title='Lab1_Task')
redis_connection = redis.Redis(host='localhost', port=6379, db=0)


def create_sorted_array(size):
	if redis_connection.exists('array'):
		array_str = redis_connection.get('array')
		array = [int(num) for num in array_str.decode().split(',')]
	else:
		array = []
		for _ in range(size):
			array.append(random.randint(1, 100))
		array.sort()
		array_str = ','.join(map(str, array))
		redis_connection.set('array', array_str)
	return array
	

def binary_search(arr, target):
    low, high = 0, len(arr)-1
    while low <= high:
        mid = (low + high) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            low = mid + 1
        else:
            high = mid - 1
    return -1


@app.get("/search/{number}")
def search_number(number: int):
    array = create_sorted_array(100)
    index = binary_search(array, number)
    return {"Index of value": index}


if __name__ == '__main__':
    run(app, host='localhost', port=8080)
